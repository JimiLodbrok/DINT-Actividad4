package com.example.mylibrary.fragments;

import com.twitter.sdk.android.core.TwitterSession;

public interface TwitterListener {
    void onTWLogin(TwitterSession session);
}
