package lodbrok.jimi.dint_actividad4.model.firebase;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lodbrok.jimi.dint_actividad4.model.persistence.DataHolder;
import lodbrok.jimi.dint_actividad4.model.persistence.Perfil;

import static android.content.ContentValues.TAG;

public class FirebaseAdmin {
    private static final FirebaseAdmin instance = new FirebaseAdmin();
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private Activity activity;
    private FirebaseUser user;
    private FirebaseLoginListener listenerLogin;
    private FirebaseDataListener listenerData;

    public FirebaseAdmin() {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference();
    }

    public static FirebaseAdmin getInstance() {
        return instance;
    }

    public void setLoginListener(FirebaseLoginListener listenerLogin) {
        this.listenerLogin = listenerLogin;
    }

    public void setDataListener(FirebaseDataListener listenerData) {
        this.listenerData = listenerData;
    }

    public boolean checkAccount() {
        return user != null;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void createAccount(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(activity, "User created.",
                                    Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "createUserWithEmail:success");
                            listenerLogin.userCreated();
                            user = mAuth.getCurrentUser();
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
    }

    public void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            Log.e(TAG, "seeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                            user = mAuth.getCurrentUser();
                            System.out.println(user.getEmail().toString());
                            listenerLogin.userSignedIn();
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Log.e(TAG, "nooooooooooooooooooooooooooooooooo ......................");
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void signOut() {
        mAuth.signOut();
    }

    public void loadData(String rama) {
        DatabaseReference databaseRef = mDatabase.child(rama);
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v("ActivityList", "perfiles---->" + dataSnapshot);
                GenericTypeIndicator<Perfil> indicator = new GenericTypeIndicator<Perfil>() {
                };
                DataHolder.getInstance().setPerfiles(dataSnapshot.getValue(indicator));
                Log.d(TAG, "Value is: " + DataHolder.getInstance().getPerfil().getNombre());
                Log.d(TAG, "SIIIIIIII------>>>: " + DataHolder.getInstance().getPerfil().getUrlProfileImg());
                listenerData.dataLoaded();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void insertData(String rama, Map<String, Object> data) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(rama, data);
        mDatabase.updateChildren(childUpdates);
    }

    public FirebaseUser getCurrentUser() {
        return user;
    }

}
