package lodbrok.jimi.dint_actividad4.model.persistence;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Perfil {
    private String nombre,uid, email;
    private String urlProfileImg;

    public Perfil() {

    }

    public Perfil(String nombre, String uid, String email) {
        this.nombre = nombre;
        this.uid = uid;
        this.email = email;
    }

    public Perfil(String nombre, String uid, String email, String urlProfileImg) {
        this.nombre = nombre;
        this.uid = uid;
        this.email = email;
        this.urlProfileImg = urlProfileImg;
    }

    @Exclude
    public Map<String,Object> toMap() {
        Map<String,Object> resultado = new HashMap<>();
        resultado.put("url",urlProfileImg);
        resultado.put("nombre",nombre);
        resultado.put("email",email);
        return resultado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUrlProfileImg() {
        return urlProfileImg;
    }

    public void setUrlProfileImg(String urlProfileImg) {
        this.urlProfileImg = urlProfileImg;
    }
}
