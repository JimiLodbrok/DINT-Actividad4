package lodbrok.jimi.dint_actividad4.model.firebase;

public interface FirebaseLoginListener {
    void userSignedIn();
    void userCreated();
}
