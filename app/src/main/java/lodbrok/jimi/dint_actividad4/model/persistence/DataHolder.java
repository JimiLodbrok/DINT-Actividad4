package lodbrok.jimi.dint_actividad4.model.persistence;

import java.util.ArrayList;

public class DataHolder {
    private static DataHolder instance = new DataHolder();
    private Perfil perfil;

    public DataHolder() {
        perfil = new Perfil();
    }

    public static DataHolder getInstance() {
        return instance;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfiles(Perfil perfil) {
        this.perfil = perfil;
    }
}
