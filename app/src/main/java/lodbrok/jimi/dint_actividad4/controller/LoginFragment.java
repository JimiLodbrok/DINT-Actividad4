package lodbrok.jimi.dint_actividad4.controller;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import lodbrok.jimi.dint_actividad4.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private Button btnLogin, btnRegister;
    private TextInputEditText edtxt_User, edtxt_Password;

    public LoginFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        btnLogin = v.findViewById(R.id.btnLogin);
        btnRegister = v.findViewById(R.id.btnRegister);

        edtxt_User = v.findViewById(R.id.edtxt_user);
        edtxt_Password = v.findViewById(R.id.edtxt_pass);

        return v;
    }

    public TextInputEditText getEdtxt_User() {
        return edtxt_User;
    }

    public TextInputEditText getEdtxt_Password() {
        return edtxt_Password;
    }

}
