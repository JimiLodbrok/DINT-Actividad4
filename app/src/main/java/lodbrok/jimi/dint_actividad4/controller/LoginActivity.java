package lodbrok.jimi.dint_actividad4.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import lodbrok.jimi.dint_actividad4.R;
import lodbrok.jimi.dint_actividad4.model.firebase.FirebaseAdmin;
import lodbrok.jimi.dint_actividad4.model.firebase.FirebaseLoginListener;
import lodbrok.jimi.dint_actividad4.model.persistence.DataHolder;
import lodbrok.jimi.dint_actividad4.model.persistence.Perfil;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, FirebaseLoginListener {
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;
    private Perfil perfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.loginFragment);
        registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentById(R.id.registerFragment);

        FirebaseAdmin.getInstance().setLoginListener(this);
        FirebaseAdmin.getInstance().setActivity(this);
        registerFragment.getBtnVolver().setOnClickListener(this);
        registerFragment.getBtnNewRegister().setOnClickListener(this);

        onReturnClicker();
    }

    public void inicializaPerfil() {
        perfil = new Perfil("Jimigar", FirebaseAdmin.getInstance().getCurrentUser().getUid(), FirebaseAdmin.getInstance().getCurrentUser().getEmail());
        FirebaseAdmin.getInstance().insertData("/perfiles/" + FirebaseAdmin.getInstance().getCurrentUser().getUid(), perfil.toMap());
    }

    public void onLoginClicked(View view) {
        if (view.getId() == R.id.btnLogin) {
            if (!loginFragment.getEdtxt_User().getText().toString().equals("") && !loginFragment.getEdtxt_Password().getText().toString().equals(""))
                FirebaseAdmin.getInstance().signIn(loginFragment.getEdtxt_User().getText().toString(), loginFragment.getEdtxt_Password().getText().toString());
            else
                Toast.makeText(this, "Fill the sign in fields",
                        Toast.LENGTH_SHORT).show();
        }
    }

    public void onNewRegisterClicker() {
        if (!registerFragment.getEdtxt_NewUser().getText().toString().equals("") && !registerFragment.getEdtxt_NewPassword().getText().toString().equals(""))
            if (registerFragment.getEdtxt_NewPassword().getText().toString().equals(registerFragment.getEdtxt_NewPassword2().getText().toString())) {
                loginFragment.getEdtxt_User().setText(registerFragment.getEdtxt_NewUser().getText().toString());
                FirebaseAdmin.getInstance().createAccount(registerFragment.getEdtxt_NewUser().getText().toString(), registerFragment.getEdtxt_NewPassword().getText().toString());
            } else
                Toast.makeText(this, "The passwords have to match",
                        Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Pleas fill all the fields.",
                    Toast.LENGTH_SHORT).show();
    }

    public void onRegisterClicked(View view) {
        if (view.getId() == R.id.btnRegister) {
            FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
            transition.hide(loginFragment);
            transition.show(registerFragment);
            transition.commit();
            clearLoginFragment();
        }
    }

    public void onReturnClicker() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(registerFragment);
        transition.show(loginFragment);
        transition.commit();
        clearRegisterFragment();
    }

    public void clearLoginFragment() {
        loginFragment.getEdtxt_User().setText("");
        loginFragment.getEdtxt_Password().setText("");
    }

    public void clearRegisterFragment() {
        registerFragment.getEdtxt_NewUser().setText("");
        registerFragment.getEdtxt_NewPassword().setText("");
        registerFragment.getEdtxt_NewPassword2().setText("");
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnNRegister) {
            onNewRegisterClicker();
        } else if (view.getId() == R.id.btnVolver)
            onReturnClicker();
    }

    @Override
    public void userSignedIn() {
        Intent secondActivity = new Intent(this, MainActivity.class);
        startActivity(secondActivity);
    }

    @Override
    public void userCreated() {
        inicializaPerfil();
        onReturnClicker();
        clearRegisterFragment();
    }
}
