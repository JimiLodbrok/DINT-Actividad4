package lodbrok.jimi.dint_actividad4.controller;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import lodbrok.jimi.dint_actividad4.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {
    private Button btnVolver, btnNewRegister;
    private TextInputEditText edtxt_NewUser, edtxt_NewPassword, edtxt_NewPassword2;
    //private EventsListener listener;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        btnVolver = v.findViewById(R.id.btnVolver);
        btnNewRegister = v.findViewById(R.id.btnNRegister);

        edtxt_NewUser = v.findViewById(R.id.edtxt_nMail);
        edtxt_NewPassword = v.findViewById(R.id.edtxt_nPass);
        edtxt_NewPassword2 = v.findViewById(R.id.edtxt_nPass2);

        return v;
    }

    public Button getBtnVolver() {
        return btnVolver;
    }

    public Button getBtnNewRegister() {
        return btnNewRegister;
    }

    public TextInputEditText getEdtxt_NewUser() {
        return edtxt_NewUser;
    }

    public TextInputEditText getEdtxt_NewPassword() {
        return edtxt_NewPassword;
    }

    public TextInputEditText getEdtxt_NewPassword2() {
        return edtxt_NewPassword2;
    }

}
