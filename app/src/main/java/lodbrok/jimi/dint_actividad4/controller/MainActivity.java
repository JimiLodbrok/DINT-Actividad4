package lodbrok.jimi.dint_actividad4.controller;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import lodbrok.jimi.dint_actividad4.R;
import lodbrok.jimi.dint_actividad4.model.firebase.FirebaseAdmin;
import lodbrok.jimi.dint_actividad4.model.firebase.FirebaseDataListener;
import lodbrok.jimi.dint_actividad4.model.persistence.DataHolder;
import lodbrok.jimi.dint_actividad4.model.persistence.Perfil;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FirebaseDataListener {
    private TextView display, nombre, email;
    private DrawerLayout drawer;
    private ImageView imgPerfil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        display = findViewById(R.id.displayText);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        nombre = header.findViewById(R.id.nombre);
        email = header.findViewById(R.id.email);
        imgPerfil = header.findViewById(R.id.imageView);
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseAdmin.getInstance().setDataListener(this);
        cargaPerfil();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_camera) {
            display.setText("Push notifications");
        } else if (id == R.id.nav_gallery) {
            display.setText("SQL Lite & Crash Reporting");
        } else if (id == R.id.nav_slideshow) {
            display.setText("Publicación App & In App Purchace");
        } else if (id == R.id.nav_share) {
            display.setText("Animaciones");
        } else if (id == R.id.nav_send) {
            display.setText("VideoJuegos & LibGDX");
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void cargaPerfil() {
        FirebaseAdmin.getInstance().loadData("perfiles/" + FirebaseAdmin.getInstance().getCurrentUser().getUid());
    }

    @Override
    public void dataLoaded() {
        nombre.setText(DataHolder.getInstance().getPerfil().getNombre());
        email.setText(DataHolder.getInstance().getPerfil().getEmail());
        if(DataHolder.getInstance().getPerfil().getUrlProfileImg() != null)
            Glide.with(this).load(DataHolder.getInstance().getPerfil().getUrlProfileImg()).into(imgPerfil);
        else
            Glide.with(this).load("https://cuadrosyvinilos.es/content/upload/master/thumb/470/59361fb8-0d4e-4ed5-a53a-6d0f91a16750.png").into(imgPerfil);
    }
}
